package com.softtek.academy.momentum.repositories;

import com.softtek.academy.momentum.model.CheckinBean;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("CheckinRepository")
public interface CheckinRepository extends JpaRepository<CheckinBean, Integer> {
}