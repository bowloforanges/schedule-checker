package com.softtek.academy.momentum.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.softtek.academy.momentum.model.CheckinBean;
import com.softtek.academy.momentum.model.EmployeeBean;
import com.softtek.academy.momentum.model.ToAddBean;
import com.softtek.academy.momentum.model.TupleBean;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class FileProcessorImpl implements FileProcessor {

    private String filePath = "";
    private int sheetIndex = 0;

    private List<EmployeeBean> employees = new ArrayList<>();
    private List<CheckinBean> checkins = new ArrayList<>();

    private static FileProcessorImpl uniqueInstance;

    private FileProcessorImpl() {

    }

    public static FileProcessorImpl getInstance() {

        if (uniqueInstance == null) {

            uniqueInstance = new FileProcessorImpl();

        }

        return uniqueInstance;
    }

    /*
     * 
     * Will read the Xls file, the path and sheet of such file can be setted with
     * setFilePath() and setSheetIndex() respectively. Outputs the excel raw data
     * converted to a list of simple objects (TupleBean) that represent each tuple.
     * 
     */

    public List<TupleBean> importXls() throws IOException {

        System.out.println("Importing XLS File...\n");

        List<TupleBean> table = new ArrayList<>();

        FileInputStream excelFile = new FileInputStream(new File(this.filePath));

        Workbook workBook = new HSSFWorkbook(excelFile);
        Sheet sheet = workBook.getSheetAt(this.sheetIndex);

        Iterator<Row> rowIterator = sheet.iterator();

        rowIterator.next();

        while (rowIterator.hasNext()) {

            Row currentRow = rowIterator.next();

            TupleBean tuple = new TupleBean(currentRow.getCell(2).getStringCellValue(), // name
                    currentRow.getCell(4).getStringCellValue(), // date
                    currentRow.getCell(6).getStringCellValue()); // check

            table.add(tuple);

        }

        table.add(new TupleBean("ZZZZ9", "9999-12-31 23:59:59", "C/Out")); // Add dummy padding to the end

        table.sort(Comparator.comparing(TupleBean::getName).thenComparing(TupleBean::getDate)
                .thenComparing(TupleBean::getCheck));

        workBook.close();
        return table;

    }

    /*
     * 
     * Will take the output list of importXls() and preprocess the data, filter whichever users
     * necessary and save the resulting data to a bean called ToAddBean, which contains the 
     * name, inDate, outDate and checkinHours fields. Its output is a list of ToAddBean objects.
     * 
     */

    public List<ToAddBean> preprocess(List<TupleBean> table) {

        System.out.println("Parsing File...\n");

        String currentName = table.get(0).getName();
        String currentDate = table.get(0).getDate();

        String actualName = "";
        String inDate = "";
        String outDate = "";

        boolean changeName = false;
        boolean changeDate = false;

        int tableSize = table.size();
        int dateCount = 1;

        List<ToAddBean> filteredTable = new ArrayList<>();

        for (int i = 1; i < tableSize; i++) {

            if (!currentName.equals(table.get(i).getName())) {

                changeName = true;
                currentName = table.get(i).getName();

            }
            if (currentDate.substring(0, 10).equals(table.get(i).getDate().substring(0, 10))) {

                dateCount++;

            } else {

                changeDate = true;
                currentDate = table.get(i).getDate();

            }

            if (dateCount >= 2 && (changeName || changeDate)) {

                ToAddBean toAdd = new ToAddBean(table.get(i - 1).getName(), table.get(i - dateCount).getDate(),
                        table.get(i - 1).getDate());

                filteredTable.add(toAdd);
                ;

                changeName = false;
                changeDate = false;
                dateCount = 1;

            } else if (dateCount == 1 && (changeName || changeDate)) {

                changeName = false;
                changeDate = false;

            }
        }

        return filteredTable;

    }

    /*
     * 
     * Will take the output list of preprocess() and separate the data into a list of EmployeeBean
     * and CheckinBean objects so they can be persisted into the database later.
     * 
     */

    public void toObject(List<ToAddBean> filteredTable) {

        System.out.println("Building Beans...\n");

        int empId = 0;
        int checkinId = 0;
        LocalTime cIn, cOut;

        String shift;

        String currentEmp = "";

        for (ToAddBean toAddBean : filteredTable) {

            if (!currentEmp.equals(toAddBean.getName())) {

                currentEmp = toAddBean.getName();
                empId++;
                EmployeeBean tempEmployee = new EmployeeBean(empId, toAddBean.getName());
                this.employees.add(tempEmployee);

                System.out.println("> " + this.employees.get(empId - 1).getEmpId() + " - "
                        + this.employees.get(empId - 1).getEmpName());

            }

            checkinId++;

            CheckinBean tempCheckin = new CheckinBean(checkinId, empId, toAddBean.getInDate(), toAddBean.getOutDate());

            cIn = LocalTime.parse(tempCheckin.getCheckinIndate().substring(11));
            cOut = LocalTime.parse(tempCheckin.getCheckinOutdate().substring(11));

            Duration duration = Duration.between(cIn, cOut);
            int totalSeconds = (int) duration.getSeconds();

            String hours = "" + ((long) (Math.floor(totalSeconds / 3600)));
            String minutes = "" + ((long) (Math.floor((totalSeconds % 3600) / 60)));
            String seconds = "" + ((long) ((totalSeconds % 3600) % 60));

            if (hours.length() == 1)
                hours = "0" + hours;
            if (minutes.length() == 1)
                minutes = "0" + minutes;
            if (seconds.length() == 1)
                seconds = "0" + seconds;

            shift = hours + ":" + minutes + ":" + seconds;
            tempCheckin.setCheckinHours(shift);

            this.checkins.add(tempCheckin);

        }

        System.out.println("Done.\n");

    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getSheetIndex() {
        return sheetIndex;
    }

    public void setSheetIndex(int sheetIndex) {
        this.sheetIndex = sheetIndex;
    }

    public List<EmployeeBean> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeBean> employees) {
        this.employees = employees;
    }

    public List<CheckinBean> getCheckins() {
        return checkins;
    }

    public void setCheckins(List<CheckinBean> checkins) {
        this.checkins = checkins;
    }

}