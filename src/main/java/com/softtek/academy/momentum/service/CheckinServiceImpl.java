package com.softtek.academy.momentum.service;

import java.util.List;

import com.softtek.academy.momentum.model.CheckinBean;
import com.softtek.academy.momentum.repositories.CheckinRepository;
import com.softtek.academy.momentum.util.FileProcessor;
import com.softtek.academy.momentum.util.FileProcessorImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("CheckinService")
public class CheckinServiceImpl implements CheckinService {

    @Autowired
    @Qualifier("CheckinRepository")
    private CheckinRepository checkinRepository;

    private FileProcessor fileProcessor = FileProcessorImpl.getInstance();

    /*
     * 
     * Persists the Checkins after obtaining them from the fileProcessor.
     * 
     */

    @Override
    public void persistCheckins() {

        List<CheckinBean> checkinTable = fileProcessor.getCheckins();

        checkinTable.stream().forEach(tuple -> {
            checkinRepository.save(tuple);
        });

    }

}