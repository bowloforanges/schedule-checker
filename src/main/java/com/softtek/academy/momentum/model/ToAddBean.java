package com.softtek.academy.momentum.model;

    /*
     * 
     * Bean that represents preprocessed excel data.
     * 
     */

public class ToAddBean {

    private String name;
    private String inDate;
    private String outDate;
    private String checkinHours;

    public ToAddBean(String name, String inDate, String outDate) {

        this.name = name;
        this.inDate = inDate;
        this.outDate = outDate;
        this.checkinHours = "not calculated";

    }

    public ToAddBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInDate() {
        return inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public String getOutDate() {
        return outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public String getCheckinHours() {
        return checkinHours;
    }

    public void setCheckinHours(String checkinHours) {
        this.checkinHours = checkinHours;
    }

}