package com.softtek.academy.momentum.repositories;

import java.util.List;

import com.softtek.academy.momentum.model.EmployeeBean;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("EmployeeRepository")
public interface EmployeeRepository extends JpaRepository<EmployeeBean, Integer> {

    /*
     * 
     * Custom JPA Query that retrieves a Employee based on its IS (name).
     * 
     */

    List<EmployeeBean> findAllByEmpName(String name);

}