package com.softtek.academy.momentum.exceptions;

public class EmployeeNotFoundException extends RuntimeException {

    /*
     * 
     * Custom Error Message
     * 
     */

  public EmployeeNotFoundException(String name) {

    super("Employee could not be found under the IS: " + name);

  }

}