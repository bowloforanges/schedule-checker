package com.softtek.academy.momentum.exceptions;

public class NonValidMonthException extends RuntimeException {

    /*
     * 
     * Custom Error Message
     * 
     */

  public NonValidMonthException(String month) {

    super("Month value '" + month
        + "' is not a valid input. Accepted inputs are following integer values (01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12)");

  }

}