package com.softtek.academy.momentum.exceptions;

public class NonValidDateFormatException extends RuntimeException {

    /*
     * 
     * Custom Error Message
     * 
     */

    public NonValidDateFormatException() {

        super("Incorrect date format, date input MUST resemble the YYYY-MM-DD format with no exceptions.");

    }

}