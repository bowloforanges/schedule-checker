package com.softtek.academy.momentum.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

    /*
     * 
     * Bean that represents the employee table of our database.
     * 
     */

@Entity
@Table(name = "employee")
public class EmployeeBean {

    @Id
    private int empId;

    @Column(name = "emp_name")
    private String empName;

    @Column(name = "emp_total_hours")
    private String empTotalHours;

    public EmployeeBean() {

    }

    public EmployeeBean(int empId, String empName) {

        this.empId = empId;
        this.empName = empName;
        this.empTotalHours = "not calculated";

    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "emp_id")
    private List<CheckinBean> checkins = new ArrayList<>();

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public List<CheckinBean> getCheckins() {
        return checkins;
    }

    public void setCheckins(List<CheckinBean> checkins) {
        this.checkins = checkins;
    }

    public String getEmpTotalHours() {
        return empTotalHours;
    }

    public void setEmpTotalHours(String empTotalHours) {
        this.empTotalHours = empTotalHours;
    }

}