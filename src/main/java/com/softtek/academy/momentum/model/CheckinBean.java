package com.softtek.academy.momentum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

    /*
     * 
     * Bean that represents the checkin table of our database.
     * 
     */

@Entity
@Table(name = "checkin")
public class CheckinBean {

    @Id
    private int checkinId;

    private int emp_id;

    @Column(name = "checkin_indate")
    private String checkinIndate;

    @Column(name = "checkin_outdate")
    private String checkinOutdate;

    @Column(name = "checkin_hours")
    private String checkinHours;

    public CheckinBean(int checkinId, int emp_id, String checkinIndate, String checkinOutdate) {

        this.checkinId = checkinId;
        this.emp_id = emp_id;
        this.checkinIndate = checkinIndate;
        this.checkinOutdate = checkinOutdate;
        this.checkinHours = "not calculated";

    }

    public CheckinBean() {

    }

    public int getCheckinId() {
        return checkinId;
    }

    public void setCheckinId(int checkinId) {
        this.checkinId = checkinId;
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getCheckinIndate() {
        return checkinIndate;
    }

    public void setCheckinIndate(String checkinIndate) {
        this.checkinIndate = checkinIndate;
    }

    public String getCheckinOutdate() {
        return checkinOutdate;
    }

    public void setCheckinOutdate(String checkinOutdate) {
        this.checkinOutdate = checkinOutdate;
    }

    public String getCheckinHours() {
        return checkinHours;
    }

    public void setCheckinHours(String checkinHours) {
        this.checkinHours = checkinHours;
    }

}