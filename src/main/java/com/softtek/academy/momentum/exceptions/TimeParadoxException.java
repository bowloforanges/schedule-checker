package com.softtek.academy.momentum.exceptions;

public class TimeParadoxException extends RuntimeException {

    /*
     * 
     * Custom Error Message
     * 
     */

    public TimeParadoxException() {

        super("Provided startDate can't be greater than endDate in selected period.");

    }

}