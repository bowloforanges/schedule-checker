package com.softtek.academy.momentum.controller;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import com.softtek.academy.momentum.exceptions.CouldNotPersistDataException;
import com.softtek.academy.momentum.exceptions.EmployeeNotFoundException;
import com.softtek.academy.momentum.exceptions.NoCheckinsAssociatedException;
import com.softtek.academy.momentum.exceptions.NoEmployeesSelectedException;
import com.softtek.academy.momentum.exceptions.NonValidDateFormatException;
import com.softtek.academy.momentum.exceptions.NonValidMonthException;
import com.softtek.academy.momentum.exceptions.TimeParadoxException;
import com.softtek.academy.momentum.model.EmployeeBean;
import com.softtek.academy.momentum.service.CheckinService;
import com.softtek.academy.momentum.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class SchedulesControllerRest {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    CheckinService checkinService;

    // REQUIRED ENDPOINTS

    /*
     * 
     * Get Request: persist excel to DataBase. DB should be empty before executing.
     * NOTES: - Excel file is stored in 'src\main\resources\static\'
     *        - FilePath and SheetIndex can be set in EmployeeServiceImplementation.java
     * 
     */

    @GetMapping("api/v1/persist")
    public String persist() throws IOException {

        try {

            employeeService.persistEmployees();
            checkinService.persistCheckins();

            return "Data was Persisted";

        } catch (Exception e) {

            throw new CouldNotPersistDataException();

        }

    }

    /*
     * 
     * Get Request: Get user with Name and month.
     * 
     */

    @GetMapping("api/v1/users/{name}/{month}") 
    public List<EmployeeBean> getEmployeeByNameMonthly(@PathVariable String name, @PathVariable String month)
            throws IOException {

        try {

            int parsedMonth = Integer.parseInt(month);

        } catch (NumberFormatException e) {

            throw new NonValidMonthException(month);

        }

        if (Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {

            throw new NonValidMonthException(month);

        } else {

            try {

                if (month.length() == 1)
                    month = "0" + month;

                List<EmployeeBean> result = employeeService.getEmployeeByNameMonthly(name, month);

                if (result.get(0).getCheckins().size() == 0) {

                    throw new NoCheckinsAssociatedException(name);

                } else
                    return result;

            } catch (IndexOutOfBoundsException e) {

                throw new EmployeeNotFoundException(name);

            }

        }

    }

    /*
     * 
     * Post Request: Send Name (IS) and date intervale, get employee by date interval.
     * 
     */

    @PostMapping("api/v1/users") 
    public List<EmployeeBean> getEmployeeByNameDateInterval(@RequestParam String name, @RequestParam String startDate,
            @RequestParam String endDate) throws ParseException {

        try {

            LocalDate sDate = LocalDate.parse(startDate);
            LocalDate eDate = LocalDate.parse(endDate);

            if (sDate.isAfter(eDate)) {
                throw new TimeParadoxException();
            }

        } catch (DateTimeParseException e) {

            throw new NonValidDateFormatException();
        }

        try {

            List<EmployeeBean> result = employeeService.getEmployeeByNameDateInterval(name, startDate, endDate);

            if (result.get(0).getCheckins().size() == 0) {

                throw new NoCheckinsAssociatedException(name);

            } else
                return result;

        } catch (IndexOutOfBoundsException e) {

            throw new EmployeeNotFoundException(name);

        }

    }

    /*
     * 
     * Get Request: Get all employees in a month.
     * 
     */

    @GetMapping("api/v1/period/{month}") 
    public List<EmployeeBean> getAllEmployeesMonthly(@PathVariable String month) throws IOException {

        try {

            int parsedMonth = Integer.parseInt(month);

        } catch (NumberFormatException e) {

            throw new NonValidMonthException(month);

        }

        if (Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {

            throw new NonValidMonthException(month);

        } else {

            if (month.length() == 1)
                month = "0" + month;

            List<EmployeeBean> result = employeeService.getAllEmployeesMonthly(month);

            if (result.size() == 0) {

                throw new NoEmployeesSelectedException();

            } else
                return result;

        }

    }

    /*
     * 
     * Post Request: Send date intervale, get all employees by date interval.
     * 
     */

    @PostMapping("api/v1/period") // Send period. Generate Error JSON
    public List<EmployeeBean> getAllEmployeesDateInterval(@RequestParam String startDate,
            @RequestParam String endDate) {

        try {

            LocalDate sDate = LocalDate.parse(startDate);
            LocalDate eDate = LocalDate.parse(endDate);

            if (sDate.isAfter(eDate)) {
                throw new TimeParadoxException();
            }

        } catch (DateTimeParseException e) {
            throw new NonValidDateFormatException();
        }

        List<EmployeeBean> result = employeeService.getAllEmployeesDateInterval(startDate, endDate);

        if (result.size() == 0) {

            throw new NoEmployeesSelectedException();

        } else
            return result;

    }

    // TEST ENDPOINTS

    /*
     * 
     * Get Request: Get all employees.
     * 
     */

    @GetMapping("api/v1/getAll")
    public List<EmployeeBean> getAllEmployees() throws IOException {

        try {

            return employeeService.getEmployees();

        } catch (Exception e) {

            throw new NoEmployeesSelectedException();

        }

    }

}
