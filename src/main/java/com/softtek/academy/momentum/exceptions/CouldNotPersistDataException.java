package com.softtek.academy.momentum.exceptions;

public class CouldNotPersistDataException extends RuntimeException {

    /*
     * 
     * Custom Error Message
     * 
     */

    public CouldNotPersistDataException() {

        super("Data was not persisted due to some error.");

    }

}