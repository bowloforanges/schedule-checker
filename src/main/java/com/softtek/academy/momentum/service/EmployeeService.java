package com.softtek.academy.momentum.service;

import java.io.IOException;
import java.util.List;

import com.softtek.academy.momentum.model.EmployeeBean;

public interface EmployeeService {

    public List<EmployeeBean> getEmployees() throws IOException;

    public List<EmployeeBean> getEmployeeByNameMonthly(String name, String month) throws IOException;

    public List<EmployeeBean> getEmployeeByNameDateInterval(String name, String startDate, String endDate);

    public List<EmployeeBean> getAllEmployeesMonthly(String month) throws IOException;

    public List<EmployeeBean> getAllEmployeesDateInterval(String startDate, String endDate);

    public List<EmployeeBean> countHours(List<EmployeeBean> allEmps);

    public void persistEmployees() throws IOException;

}