package com.softtek.academy.momentum.exceptions;

public class NoCheckinsAssociatedException extends RuntimeException {

    /*
     * 
     * Custom Error Message
     * 
     */

  public NoCheckinsAssociatedException(String name) {

    super("Employee with IS: " + name + " has no shifts registered for the selected period of time.");

  }

}