package com.softtek.academy.momentum.controller;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

import com.softtek.academy.momentum.exceptions.CouldNotPersistDataException;
import com.softtek.academy.momentum.exceptions.EmployeeNotFoundException;
import com.softtek.academy.momentum.exceptions.NoCheckinsAssociatedException;
import com.softtek.academy.momentum.exceptions.NoEmployeesSelectedException;
import com.softtek.academy.momentum.exceptions.NonValidDateFormatException;
import com.softtek.academy.momentum.exceptions.NonValidMonthException;
import com.softtek.academy.momentum.exceptions.TimeParadoxException;
import com.softtek.academy.momentum.model.EmployeeBean;
import com.softtek.academy.momentum.service.CheckinService;
import com.softtek.academy.momentum.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SchedulesController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    CheckinService checkinService;

    // REQUIRED ENDPOINTS

    /*
     * 
     * Get Request: persist excel to DataBase. DB should be empty before executing.
     * NOTES: - Excel file is stored in 'src\main\resources\static\'
     *        - FilePath and SheetIndex can be set in EmployeeServiceImplementation.java
     * 
     */

    @GetMapping("start")
    public String startPage(){
        return "index";
    }

    @GetMapping("users/persist")
    public String persist() throws IOException {

        try {

            employeeService.persistEmployees();
            checkinService.persistCheckins();

            return "Data was Persisted";

        } catch (Exception e) {

            throw new CouldNotPersistDataException();

        }

    }

    /*
     * 
     * Get Request: Get user with Name and month.
     * 
     */

    @GetMapping("user/month") 
    public String getEmployeeByNameMonthly(Model model, String name, String month)
            throws IOException {

        try {

            int parsedMonth = Integer.parseInt(month);

        } catch (NumberFormatException e) {

            throw new NonValidMonthException(month);

        }

        if (Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {

            throw new NonValidMonthException(month);

        } else {

            try {

                if (month.length() == 1)
                    month = "0" + month;

                List<EmployeeBean> result = employeeService.getEmployeeByNameMonthly(name, month);

                if (result.get(0).getCheckins().size() == 0) {

                    throw new NoCheckinsAssociatedException(name);

                } else {

                    model.addAttribute("employees", result);
                    model.addAttribute("name", name);

                    return "views/getByNameAndMonthForm";

                }

            } catch (IndexOutOfBoundsException e) {

                throw new EmployeeNotFoundException(name);

            }

        }

    }

    /*
     * 
     * Post Request: Send Name (IS) and date intervale, get employee by date interval.
     * 
     */

    @PostMapping("user") 
    public String getEmployeeByNameDateInterval(Model model, String name, String startDate, String endDate) throws ParseException {

        try {

            LocalDate sDate = LocalDate.parse(startDate);
            LocalDate eDate = LocalDate.parse(endDate);

            if (sDate.isAfter(eDate)) {
                throw new TimeParadoxException();
            }

        } catch (DateTimeParseException e) {

            throw new NonValidDateFormatException();
        }

        try {

            List<EmployeeBean> result = employeeService.getEmployeeByNameDateInterval(name, startDate, endDate);

            if (result.get(0).getCheckins().size() == 0) {

                throw new NoCheckinsAssociatedException(name);

            } else {

                model.addAttribute("employees", result);
                model.addAttribute("name", name);
                model.addAttribute("sDate", startDate);
                model.addAttribute("eDate", endDate);

                return "views/getByNameAndIntervalForm";
            }

        } catch (IndexOutOfBoundsException e) {

            throw new EmployeeNotFoundException(name);

        }

    }

    /*
     * 
     * Get Request: Get all employees in a month.
     * 
     */

    @GetMapping("period/month") 
    public String getAllEmployeesMonthly(Model model, String month) throws IOException {

        try {

            int parsedMonth = Integer.parseInt(month);

        } catch (NumberFormatException e) {

            throw new NonValidMonthException(month);

        }

        if (Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {

            throw new NonValidMonthException(month);

        } else {

            if (month.length() == 1)
                month = "0" + month;

            List<EmployeeBean> result = employeeService.getAllEmployeesMonthly(month);

            if (result.size() == 0) {

                throw new NoEmployeesSelectedException();

            } else {

                model.addAttribute("employees", result);
                model.addAttribute("month", month);
                return "views/getAllByMonthForm";

            }

        }

    }

    /*
     * 
     * Post Request: Send date intervale, get all employees by date interval.
     * 
     */

    @PostMapping("period") // Send period. Generate Error JSON
    public String getAllEmployeesDateInterval(Model model, String startDate,
            String endDate) {

        try {

            LocalDate sDate = LocalDate.parse(startDate);
            LocalDate eDate = LocalDate.parse(endDate);

            if (sDate.isAfter(eDate)) {
                throw new TimeParadoxException();
            }

        } catch (DateTimeParseException e) {
            throw new NonValidDateFormatException();
        }

        List<EmployeeBean> result = employeeService.getAllEmployeesDateInterval(startDate, endDate);

        if (result.size() == 0) {

            throw new NoEmployeesSelectedException();

        } else {

            model.addAttribute("employees", result);
            model.addAttribute("sDate", startDate);
            model.addAttribute("eDate", endDate);

            return "views/getAllByIntervalForm";

        }           

    }

    // TEST ENDPOINTS

    /*
     * 
     * Get Request: Get all employees.
     * 
     */

    @GetMapping("getAll")
    public String getAllEmployees(Model model) throws IOException {

        try {

            List<EmployeeBean> allEmps = employeeService.getEmployees();
            model.addAttribute("employees", allEmps);
            return "views/getAll";

        } catch (Exception e) {

            throw new NoEmployeesSelectedException();

        }

    }

    // VIEW ENDPOINTS

    @GetMapping("getByNameAndMonthForm")
    public String getByNameAndMonthForm(){
        return "views/getByNameAndMonthForm";
    }

    @GetMapping("getByNameAndIntervalForm")
    public String getByNameAndIntervalForm() {
        return "views/getByNameAndIntervalForm";
    }

    @GetMapping("getAllByMonthForm")
    public String getAllByMonthForm() {
        return "views/getAllByMonthForm";
    }

    @GetMapping("getAllByIntervalForm")
    public String getAllByIntervalForm(){
        return "views/getAllByIntervalForm";
    }
}