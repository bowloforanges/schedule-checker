package com.softtek.academy.momentum.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.List;

import com.softtek.academy.momentum.model.EmployeeBean;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class EmployeeServiceImplTest {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    CheckinService checkinService;

    /*
     * 
     * First Test will retrieve the full list of employees and will make sure the IS
     * (name) of the first registered employee is "AAAC1".
     * 
     * Second Test will retrieve the registry for employee with IS (name) "OCO1" and
     * its respective checkins for the period between "2019-03-28" and "2019-04-02".
     * then it will make sure the total hours in this period of time corresponding
     * to this particular user is "20:27:07".
     * 
     */

    @Test
    @Transactional
    public void getAllEmployeesAndCompareFirstEmployeeNameTest() throws IOException {

        // Setup
        List<EmployeeBean> empList;
        String expectedName = "AAAC1";

        // Execute
        empList = employeeService.getEmployees();
        String actualName = empList.get(0).getEmpName();

        // Validate
        assertNotNull(empList);
        assertEquals(expectedName, actualName);

    }

    @Test
    @Transactional
    public void getHoursByPeriodAndEmployeeTest() throws IOException {

        // Setup
        List<EmployeeBean> empList;
        String expectedHours = "20:27:07";

        // Execute
        empList = employeeService.getEmployeeByNameDateInterval("OCO1", "2019-03-28", "2019-04-02");
        String actualHours = empList.get(0).getEmpTotalHours();

        // Validate
        assertNotNull(empList);
        assertEquals(expectedHours, actualHours);

    }

}