package com.softtek.academy.momentum.exceptions;

public class NoEmployeesSelectedException extends RuntimeException {

    /*
     * 
     * Custom Error Message
     * 
     */

    public NoEmployeesSelectedException() {

        super("No employees were found to have registers on this period. If you're searching by month, it may be helpful to try again making sure month format 'MM' is being used correctly.");

    }

}