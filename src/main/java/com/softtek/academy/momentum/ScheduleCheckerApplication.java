package com.softtek.academy.momentum;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScheduleCheckerApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(ScheduleCheckerApplication.class, args);

	}

}
