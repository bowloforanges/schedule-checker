package com.softtek.academy.momentum.exceptions;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

    /*
     * 
     * ControllerAdvice for exception handling and error JSON dispatch.
     * 
     */

@ControllerAdvice(basePackages = "com.softtek.academy.momentum.controller", annotations = RestController.class)
public class GlobalControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EmployeeNotFoundException.class)
    public void EmployeeNotFound(HttpServletResponse response) throws IOException {

        response.sendError(HttpStatus.NOT_FOUND.value());

    }

    @ExceptionHandler(NonValidMonthException.class)
    public void NonValidMonthException(HttpServletResponse response) throws IOException {

        response.sendError(HttpStatus.NOT_ACCEPTABLE.value());

    }

    @ExceptionHandler(NoCheckinsAssociatedException.class)
    public void NoCheckinsAssociatedException(HttpServletResponse response) throws IOException {

        response.sendError(HttpStatus.NOT_FOUND.value());

    }

    @ExceptionHandler(NonValidDateFormatException.class)
    public void NonValidDateFormatException(HttpServletResponse response) throws IOException {

        response.sendError(HttpStatus.NOT_ACCEPTABLE.value());

    }

    @ExceptionHandler(TimeParadoxException.class)
    public void TimeParadoxException(HttpServletResponse response) throws IOException {

        response.sendError(HttpStatus.NOT_ACCEPTABLE.value());

    }

    @ExceptionHandler(NoEmployeesSelectedException.class)
    public void NoEmployeesSelectedException(HttpServletResponse response) throws IOException {

        response.sendError(HttpStatus.NOT_FOUND.value());

    }

    @ExceptionHandler(CouldNotPersistDataException.class)
    public void CouldNotPersistDataException(HttpServletResponse response) throws IOException {

        response.sendError(HttpStatus.NOT_FOUND.value());

    }

}