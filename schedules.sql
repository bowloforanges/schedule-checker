-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema schedules
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema schedules
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `schedules` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `schedules` ;

-- -----------------------------------------------------
-- Table `schedules`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedules`.`employee` (
  `emp_id` INT(11) NOT NULL AUTO_INCREMENT,
  `emp_name` VARCHAR(45) NOT NULL,
  `emp_total_hours` VARCHAR(20) NULL DEFAULT 'not calculated',
  PRIMARY KEY (`emp_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 66
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `schedules`.`checkin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedules`.`checkin` (
  `checkin_id` INT(11) NOT NULL AUTO_INCREMENT,
  `checkin_indate` DATETIME NOT NULL,
  `checkin_outdate` DATETIME NOT NULL,
  `emp_id` INT(11) NOT NULL,
  `checkin_hours` VARCHAR(20) NOT NULL DEFAULT 'not calculated',
  PRIMARY KEY (`checkin_id`),
  INDEX `fk_checkin_employee_idx` (`emp_id` ASC) VISIBLE,
  CONSTRAINT `fk_checkin_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `schedules`.`employee` (`emp_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1940
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
