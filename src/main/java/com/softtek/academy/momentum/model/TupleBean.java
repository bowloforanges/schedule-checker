package com.softtek.academy.momentum.model;

    /*
     * 
     * Bean that represents excel file raw tuples.
     * 
     */

public class TupleBean {

    private String name;
    private String date;
    private String check;

    public TupleBean(String name, String date, String check) {

        this.name = name;
        this.date = date;
        this.check = check;

    }

    public TupleBean() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

}