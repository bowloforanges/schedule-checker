package com.softtek.academy.momentum.util;

import java.io.IOException;
import java.util.List;

import com.softtek.academy.momentum.model.CheckinBean;
import com.softtek.academy.momentum.model.EmployeeBean;
import com.softtek.academy.momentum.model.ToAddBean;
import com.softtek.academy.momentum.model.TupleBean;

public interface FileProcessor {

    public List<TupleBean> importXls() throws IOException;

    public List<ToAddBean> preprocess(List<TupleBean> table);

    public void toObject(List<ToAddBean> filteredTable);

    public String getFilePath();

    public void setFilePath(String filepath);

    public int getSheetIndex();

    public void setSheetIndex(int sheetIndex);

    public List<EmployeeBean> getEmployees();

    public void setEmployees(List<EmployeeBean> employees);

    public List<CheckinBean> getCheckins();

    public void setCheckins(List<CheckinBean> checkins);

}