package com.softtek.academy.momentum.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import com.softtek.academy.momentum.model.CheckinBean;
import com.softtek.academy.momentum.model.EmployeeBean;
import com.softtek.academy.momentum.repositories.EmployeeRepository;
import com.softtek.academy.momentum.util.FileProcessor;
import com.softtek.academy.momentum.util.FileProcessorImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("EmployeeService")
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    @Qualifier("EmployeeRepository")
    private EmployeeRepository employeeRepository;

    private FileProcessor fileProcessor = FileProcessorImpl.getInstance();

    /*
     * 
     * Persists the Employees after obtaining them from the fileProcessor.
     * Set here FilePath and SheetIndex
     * 
     */

    public void persistEmployees() throws IOException {

        System.out.println("Persisting beans...\n");
        fileProcessor.setFilePath("src\\main\\resources\\static\\Registros Momentum.xls");
        // fileProcessor.setFilePath("src\\main\\resources\\static\\test.xls");
        fileProcessor.setSheetIndex(0);
        fileProcessor.toObject(fileProcessor.preprocess(fileProcessor.importXls()));

        List<EmployeeBean> employeeTable = fileProcessor.getEmployees();

        employeeTable.stream().forEach(tuple -> {
            employeeRepository.save(tuple);
        });

        System.out.println("\nDone.");
    }

    /*
     * 
     * Gets All Employees.
     * 
     */

    @Override
    public List<EmployeeBean> getEmployees() throws IOException {

        return countHours(employeeRepository.findAll());

    }

    /*
     * 
     * Gets an employee registry based on its IS (name) and a month value.
     * 
     */

    @Override
    public List<EmployeeBean> getEmployeeByNameMonthly(String name, String month) throws IOException {

        List<EmployeeBean> empMonthly = employeeRepository.findAllByEmpName(name);

        empMonthly.get(0).getCheckins()
                .removeIf((checkin) -> !checkin.getCheckinIndate().substring(5, 7).equals(month));

        return countHours(empMonthly);

    }

    /*
     * 
     * Gets an employee registry based on its IS (name) and a date intreval.
     * 
     */

    @Override
    public List<EmployeeBean> getEmployeeByNameDateInterval(String name, String startDate, String endDate) {

        List<EmployeeBean> empInterval = employeeRepository.findAllByEmpName(name);

        LocalDate sDate = LocalDate.parse(startDate).minusDays(1);
        LocalDate eDate = LocalDate.parse(endDate).plusDays(1);

        empInterval.get(0).getCheckins()
                .removeIf((checkin) -> !((LocalDate.parse(checkin.getCheckinIndate().substring(0, 10)).isAfter(sDate))
                        && (LocalDate.parse(checkin.getCheckinIndate().substring(0, 10)).isBefore(eDate))));

        return countHours(empInterval);

    }

    /*
     * 
     * Gets all employees based on a month value.
     * 
     */

    @Override
    public List<EmployeeBean> getAllEmployeesMonthly(String month) throws IOException {

        List<EmployeeBean> allEmpsMonthly = employeeRepository.findAll();

        for (EmployeeBean employeeBean : allEmpsMonthly) {

            employeeBean.getCheckins().removeIf((checkin) -> !checkin.getCheckinIndate().substring(5, 7).equals(month));

        }

        allEmpsMonthly.removeIf((emp) -> emp.getCheckins().size() == 0);

        return countHours(allEmpsMonthly);

    }

    /*
     * 
     * Gets all employees based on a date interval.
     * 
     */

    @Override
    public List<EmployeeBean> getAllEmployeesDateInterval(String startDate, String endDate) {

        List<EmployeeBean> allEmpsInterval = employeeRepository.findAll();

        LocalDate sDate = LocalDate.parse(startDate).minusDays(1);
        LocalDate eDate = LocalDate.parse(endDate).plusDays(1);

        for (EmployeeBean employeeBean : allEmpsInterval) {

            employeeBean.getCheckins().removeIf(
                    (checkin) -> !((LocalDate.parse(checkin.getCheckinIndate().substring(0, 10)).isAfter(sDate))
                            && (LocalDate.parse(checkin.getCheckinIndate().substring(0, 10)).isBefore(eDate))));

        }

        allEmpsInterval.removeIf((emp) -> emp.getCheckins().size() == 0);

        return countHours(allEmpsInterval);
    }

    /*
     * 
     * Assigns the total hour count by employee based on a input list of employees.
     * 
     */

    @Override
    public List<EmployeeBean> countHours(List<EmployeeBean> allEmps) {

        for (EmployeeBean employee : allEmps) {

            List<CheckinBean> checkins = employee.getCheckins();

            String[] sections;
            int totalSeconds = 0;

            for (CheckinBean checkin : checkins) {

                sections = checkin.getCheckinHours().split(":");

                int hoursToSeconds = (Integer.parseInt(sections[0]) * 3600) + (Integer.parseInt(sections[1]) * 60)
                        + (Integer.parseInt(sections[2]));

                totalSeconds += hoursToSeconds;

            }

            String toHours = "" + (int) (totalSeconds / 3600);
            String toMinutes = "" + (int) ((totalSeconds / 60) % 60);
            String toSeconds = "" + (int) (totalSeconds % 60);

            if (toHours.length() == 1)
                toHours = "0" + toHours;
            if (toMinutes.length() == 1)
                toMinutes = "0" + toMinutes;
            if (toSeconds.length() == 1)
                toSeconds = "0" + toSeconds;

            String secondsToHours = toHours + ":" + toMinutes + ":" + toSeconds;

            employee.setEmpTotalHours(secondsToHours);

        }

        return allEmps;
    }

}