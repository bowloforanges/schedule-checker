package com.softtek.academy.momentum.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.List;

import com.softtek.academy.momentum.model.ToAddBean;
import com.softtek.academy.momentum.model.TupleBean;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FileProcessorImplTest {

    /*
     * 
     * First test checks that the Singleton implementation for the FileProcessor
     * util is indeed working as intended by comparing the hash codes of 2 different
     * variables of this class
     * 
     * Second test checks that FileProcessor is importing the excel file correctly
     * and verifying this happens with an assertNotNull validation and comparing the
     * expected name of the first tuple to its real value.
     * 
     * Third test does just as the second but in this case for a different stage of
     * the file processing pipeline.
     * 
     */

    @Test
    public void checkSingletonTest() {

        // Setup
        FileProcessorImpl fileProcessor1 = FileProcessorImpl.getInstance();
        FileProcessorImpl fileProcessor2 = FileProcessorImpl.getInstance();

        // Execute
        int expectedHash = fileProcessor1.hashCode();
        int actualHash = fileProcessor2.hashCode();

        // Validate
        assertEquals(expectedHash, actualHash);

        // Hash value of both fileProcessor are equal because its class is a Singleton.
    }

    @Test
    public void ensureCorrectXlsFileImportTest() throws IOException {

        // Setup
        FileProcessorImpl fileProcessor = FileProcessorImpl.getInstance();
        fileProcessor.setFilePath("src\\main\\resources\\static\\Registros Momentum.xls");
        fileProcessor.setSheetIndex(0);

        List<TupleBean> rawXlsImportedData;

        String actualName;
        String expectedName = "AAAC1";

        // Execute
        rawXlsImportedData = fileProcessor.importXls();
        actualName = rawXlsImportedData.get(0).getName();

        // Validate
        assertNotNull(rawXlsImportedData);
        assertEquals(expectedName, actualName);
    }

    @Test
    public void ensurePreprocessingIsWorkingTest() throws IOException {

        // Setup
        FileProcessorImpl fileProcessor = FileProcessorImpl.getInstance();
        fileProcessor.setFilePath("src\\main\\resources\\static\\Registros Momentum.xls");
        fileProcessor.setSheetIndex(0);

        String actualInDate;
        String expectedInDate = "2019-01-02 06:43:41";

        List<TupleBean> rawXlsImportedData = fileProcessor.importXls();

        List<ToAddBean> processedData;

        // Execute
        processedData = fileProcessor.preprocess(rawXlsImportedData);
        actualInDate = processedData.get(0).getInDate();

        // Validate
        assertNotNull(processedData);
        assertEquals(expectedInDate, actualInDate);

    }
}